<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use AppBundle\Form\postType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;



class PostsController extends Controller
{
    /**
     * @Route("/profile")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postAction(Request $request)
    {
        $user = $this->getUser();
        $post = new Post();
        $form = $this->createForm(postType::class, $post);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $post = $form->getData();
            $user =$this->getUser();
            $post->setAuthor($user);
            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();
            return $this->redirectToRoute("app_posts_post");
        }
        $followers = $user->getFollowing();

        dump($followers);

        return $this->render('@App/Posts/profile.html.twig', array(
            'form' => $form->createView(),
            'user' => $user,
            'followers' => $followers
        ));
    }

    /**
     * @Route("/post/{id}", requirements={"id": "\d+"})
     * @Method({"GET","HEAD"})
     * @param $id integer
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function likeAction(int $id, Request $request)
    {
        $user = $this->getUser();
        $post = $this->getDoctrine()
            ->getRepository('AppBundle:Post')
            ->find($id);
        $em = $this->getDoctrine()->getManager();
        if ($post->contains($user)){
            $post->removeLiker($user);
            $em->flush();
            return $this->redirect($request->server->get('HTTP_REFERER'));
        }
        $post->addLiker($user);
        $em->flush();
        dump($request->getBasePath());
        dump($request->getBaseUrl());
        return $this->redirect($request->server->get('HTTP_REFERER'));
    }

    /**
     * @Route("/tape")
     */
    public function tapeAction()
    {
        $posts = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Post')
            ->findAll();
        $user = $this->getUser();
        return $this->render('@App/Posts/tape.html.twig', [
           'posts' => $posts,
            'user' => $user
        ]);

    }

    /**
     * @Route("/follower/{id}", requirements={"id": "\d+"})
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function followerAction($id, Request $request){
        $user = $this->getUser();
        $follower = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->find($id);
        $em = $this->getDoctrine()->getManager();
        if ($user->containsUser($follower)){
            $user->removeLiker($follower);
            $em->flush();
            return $this->redirect($request->server->get('HTTP_REFERER'));
        }
        $user->addFollower($follower);
        $em->flush();

        return $this->redirect($request->server->get('HTTP_REFERER'));
    }
}
