<?php

namespace AppBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;


/**
 * Post
 *
 * @ORM\Table(name="post")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PostRepository")
 * @Vich\Uploadable
 */
class Post
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="post_file", fileNameProperty="postImage")
     *
     * @var File
     *
     */
    private $image;

    /**
     * @ORM\Column(name="image", type="string")
     * @var string
     */
    private $postImage;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="posts")
     * @ORM\JoinColumn(name="author", referencedColumnName="id")
     */
    private $author;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="User", indexBy="id", cascade={"persist"})
     */
    public $likers;

    public function __construct()
    {
        $this->likers = new ArrayCollection();
    }

    public function addLiker(User $liker)
    {
        $this->likers->add($liker);
    }

    public function getCountLikers()
    {
        return $this->likers->count();
    }

    public function contains(User $liker) {
        return $this->likers->contains($liker);
    }

    /**
     * @param User $liker
     */
    public function removeLiker(User $liker)
    {
        $this->likers->removeElement($liker);
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set image
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image

     *
     * @return Post
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return File|null

     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @return User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param User $author
     * @return Post
     */
    public function setAuthor($author)
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @return string
     */
    public function getPostImage()
    {
        return $this->postImage;
    }

    /**
     * @param string $postImage
     * @return Post
     */
    public function setPostImage($postImage)
    {
        $this->postImage = $postImage;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getLikers(): ArrayCollection
    {
        return $this->likers;
    }


}

