<?php
namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;


/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 * @Vich\Uploadable
 */

class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     *
     * @ORM\Column(name="avatar", type="text", nullable=true, unique=false)
     */
    private $avatar;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Post", mappedBy="author")
     */
    private $posts;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="User")
     */
    private $following;

    public function __construct()
    {
        parent::__construct();
        $this->posts = new ArrayCollection();
        $this->following = new ArrayCollection();
    }

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="avatar_file", fileNameProperty="avatar")
     *
     * @var File
     */
    private $imageFile;


    /**
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return User
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;
        return $this;
    }

    /**
     * @return File|null
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }


    /**
     * @param Post $post
     */
    public function removePosts(Post $post)
    {
        $this->posts->removeElement($post);
    }

    /**
     * @param Post $post
     */
    public function addPosts(Post $post)
    {
        $this->posts->add($post);
    }

    /**
     * @return ArrayCollection
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param string $avatar
     * @return User
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
        return $this;
    }

    /**
     * @param User $user
     */
    public function addFollower(User $user)
    {
        $this->following->add($user);
    }

    /**
     * @return ArrayCollection
     */
    public function getFollowing()
    {
        return $this->following;
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    public function containsUser(User $user) {
        return $this->following->contains($user);
    }

    /**
     * @param User $follower
     */
    public function removeLiker(User $follower)
    {
        $this->following->removeElement($follower);
    }


}
